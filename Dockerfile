FROM codingwell/rpi-raspbian-qemu

MAINTAINER takashi FUJIWARA
RUN [ "cross-build-start" ]
RUN apt-get update \&&
	apt-get -y upgrade
RUN [ "cross-build-end" ]